from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, CreateAPIView

from .models import Employeer
from .serializers import EmployeerSerializer




"Get all employers on database"
class EmployeerList(ListAPIView):
    queryset = Employeer.objects.all()
    serializer_class = EmployeerSerializer
        

"Create a new employer"
class EmployeerPost(CreateAPIView):
    queryset = EmployeerSerializer
    serializer_class = EmployeerSerializer


"Get employer detail, update or delete him"
class EmployeerManage(APIView):
    "get employer detail"
    def get(self, request, pk):
        employeer = Employeer.objects.get(pk=pk)
        serializer = EmployeerSerializer(employeer)
        return Response(serializer.data)

    "update employer"
    def put(self, request, pk):
        employeer = Employeer.objects.get(pk=pk)
        serializer = EmployeerSerializer(employeer, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    "delete employer"
    def delete(self, request, pk):
        employeer = Employeer.objects.get(pk=pk)
        employeer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


