from rest_framework import serializers

from apps.core.models import Employeer




class EmployeerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employeer
        fields = ('name', 'email', 'department')