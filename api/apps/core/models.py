from django.db import models
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _




class Employeer(models.Model):
    name = models.CharField(_('nome'), max_length=100)
    email = models.EmailField(_('e-mail'))
    department = models.CharField(_('departamento'), max_length=150)

    class Meta:
        verbose_name = _('empregado')
        verbose_name_plural = _('empregados')
        
    def __str__(self):
        return mark_safe(u"%s <%s>") % (escape(self.name), escape(self.email))
