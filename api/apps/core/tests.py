from django.test import TestCase

from .models import Employeer




class EmployeerModelTest(TestCase):

    def setUp(self):
        self.employee = Employeer.objects.create(name="fernando", email="fernando@email.com", department="Architecture")

        all_employe_in_data = Employeer.objects.all()
        self.assertEqual(len(all_employe_in_data), 1)

        only_employe_in_data = all_employe_in_data[0]
        self.assertEqual(only_employe_in_data, self.employee)


