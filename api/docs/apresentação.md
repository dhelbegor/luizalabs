Como o teste é bastante simples porém com uma pitada de vamos lá, faça mais do que criar uma simples api, comecei por utilizar docker e docker-compose, por ser uma ferramenta bastante versátil. porém, para se utilizar está API não precisa necessariamente de docker, pode-se usar virtualenv também, logo a baixo deixo ambas as maneiras citas a cima de como utiliza-los.



METODO 1:
	Docker:
	para utilizar o projeto via docker, primeiro devemos instalar o docker em nossa maquina, logo a baixo segue o link para instalação do mesmo via site oficial.

	https://docs.docker.com/engine/installation/

	a instalação e bastante simples basta seguir o passo a passo.

	para ver se o docker foi instalado em sua maquina basta rodar: $ docker -v
	se aparacer algo como: Docker version 17.03.0-ce, build 60ccb22, quer dizer que está tudo ok.

	Docker-compose:
	para se instalar o docker-compose basta seguir o passo a passo que se encontra no site do mesmo.

	https://docs.docker.com/compose/install/

	para saber se o docker-compose foi instalado basta rodar: $ docker-compose -v
	se aparecer algo como: docker-compose version 1.11.2, build dfed245, está tudo ok.


	Rodando a aplicação:
	depois de instalada as dependências requeridas logo a cima, podemos agora rodar nossa aplicação.


	$ docker-compose up # o mesmo irá baixar também a imagem necessaria para uso do docker

	se tudo der certo vai aparecer algo como:

		Starting luizalabs_api_1
		Attaching to luizalabs_api_1
		api_1  | No changes detected
		api_1  | Operations to perform:
		api_1  |   Apply all migrations: admin, auth, contenttypes, core, sessions
		api_1  | Running migrations:
		api_1  |   No migrations to apply.


	criando superuser para acessar o admin do projeto, para isso ou você derruba o servidor que está usando atraves de Ctrl C e digita o comando a baixo, ou abre uma nova aba do terminal e digita o comando a seguir.

	$ docker-compose run --rm api bash -c "python manage.py createsuperuser"

	faça como aparece nas opções a baixo.
	Username (leave blank to use 'root'): admin
	Email address:      # pode deixar em branco
	Password: luizalabs
	Password (again): luizalabs

	para subir o servidor novamente basta utilizar: $ docker-compose up

	pronto, está tudo ok para utilizar a api, vide[ACESSANDO API VIA CURL]


METODO 2:
	PIP:
	esta opção de instalação que estou deixando e para uso da distribuição linux/ubuntu.

	para instalar o pip basta rodar: $ sudo apt-get install python-pip   na sua maquina.

	logo após ter instalado o mesmo rode: $ pip -V
	se aparecer algo como: pip 9.0.1 from /home/user/.local/lib/python2.7/site-packages (python 2.7), significa que está tudo ok com o pip.


	VIRTUALENV:
	para instalar o virtualenv e bastante simples, basta rodar: $ pip install virtualenv
	logo após rode: $ virtualenv --version, se tudo ocorrer bem deve aparecer algo como: 15.1.0.

	Rodando a aplicação:
	depois de instaladas todas as dependências a cima, vamos colocar a mão na massa.

	acesse a pasta luizalabs/api.
	$ cd luizalabs/api

	rode o comando do virtualenv para criar um ambiente isolado para o projeto.
	$ virtualenv -p python3 .venv

	com este comando logo a cima iremos criar um ambiente isolado, o parametro python3, indica que vamos utilizar python3 neste projeto, e o parametro .venv será a pasta que contem todo o ambiente isolado.

	logo após rode: $ source .venv/bin/activate
	este comando irá ativar nosso ambiente virtual.
	se tudo ocorrer bem vai aparecer algo como (.venv)/luizalabs/api:~$


	agora precisamos atualizar o pip que está contido neste ambiente virtual e logo após instalar as depêndencias do projeto, segue a baixo.

	atualizando o pip.
	(.venv)/luizalabs/api:~$ pip install -U pip

	instalando as depêndencias.
	(.venv)/luizalabs/api:~$ pip install -r requirements/base.pip

	depois de atualizado o pip e instalado as depêndencias basta rodar o projeto, para isso precisamos de três(3) comandos, são eles.

	(.venv)/luizalabs/api:~$ make user

	faça como aparece nas opções a baixo.
	Password: luizalabs
	Password (again): luizalabs

	(.venv)/luizalabs/api:~$ make migrate

	(.venv)/luizalabs/api:~$ make run

	pronto, está tudo ok para utilizar a api, vide[ACESSANDO API VIA CURL]


ACESSANDO API VIA CURL:
	para acessar a api estou usando curl, se não o tem instalado vamos instala-lo, caso contrario pule essa parte.

	$ sudo apt-get update && sudo apt-get install -y curl

	logo após verifique se o mesmo foi instalado em sua maquina com o seguinte comando.

	$ curl -V

	se aparecer algo do como:
	curl 7.50.1 (x86_64-pc-linux-gnu) libcurl/7.50.1 GnuTLS/3.5.3 zlib/1.2.8 libidn/1.33 librtmp/2.3

	significa que está tudo ok.


	ACESSANDO API:
		Method POST:
		curl -X POST  http://localhost:8000/employee/create/ -d "name=Arnaldo Pereira&email=arnaldo@luizalabs.com&department=Architecture"

		Method GET:
		curl -H "Content-Type: application/javascript" http://localhost:8000/employee/

		Method PUT:
		curl -X PUT http://localhost:8000/employee/1 -d "name=Arnaldo Pereira&email=arnaldo@luizalabs.com&department=E-commerce"

		Method DELETE:
		curl -X DELETE http://localhost:8000/employee/1

