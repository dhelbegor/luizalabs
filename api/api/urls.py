# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin



from apps.core import views


urlpatterns = [
    url(r'^employee/$', views.EmployeerList.as_view(), name='employee_list'),
    url(r'^employee/create/$', views.EmployeerPost.as_view(), name='employeer_create'),
    url(r'^employee/(?P<pk>[0-9]+)$', views.EmployeerManage.as_view(), name='employee_detail'),
    url(r'^admin/', admin.site.urls),
    ]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)