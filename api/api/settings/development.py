# -*- coding: utf-8 -*-

import os
from .base import *



BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True


SECRET_KEY = 'ms9ljs^gpb&k%i90ht*vvahw1*(58v51etj_gd^wslww$rafy9'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'database.db'),
    }
}